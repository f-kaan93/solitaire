import React from 'react';
import { StyleSheet, View, TouchableWithoutFeedback, Animated, ScrollView, Button, Text } from 'react-native';
import Card from './components/Card';
import Draggable from './components/Draggable';

import { initialize, deal, playStockToPile, playStockToFoundation, playPileToFoundation, playPileToPile } from './model';

export default function Main() {
    const init = initialize();
    const [cards, setCards] = React.useState(init);

    function handleDeal() {
        setCards({ ...cards, stock: deal(cards.stock) });
    }

    function handleStockRelease(evt, gestureState, pan) {
        function springAnim() {
            Animated.spring(
                pan,
                {
                    toValue: { x: 0, y: 0 },
                    useNativeDriver: true
                } // Back to zero
            ).start();
        }
        function stop() {
            pan.setValue({
                x: 0,
                y: 0
            })
        }
        const yPos = gestureState.moveY;
        const xPos = gestureState.moveX;

        // Check if it is in the foundation zone
        if (yPos < 350 && xPos < 150) {
            const res = playStockToFoundation(cards);
            if (res.valid) {
                stop();
                setCards({ ...res.cards })
                return;
            } else {
                springAnim();
                return;
            }
        }

        // Pile zone

        // Substract margin (60), divide card length (60) + bottom margin (6)
        // to figure out which pile 
        const pileIndex = Math.floor((yPos - 60) / 66)
        const isPileValid = pileIndex >= 0 && pileIndex <= 6;

        if (!isPileValid) {
            springAnim();
            return;
        }
        const res = playStockToPile(cards, pileIndex);

        if (!res.valid) {
            springAnim();
            return;
        }
        stop();
        setCards({ ...res.cards })
    }
    function handlePileRelease(evt, gestureState, pan, playCardIndex, playPileIndex) {
        function springAnim() {
            Animated.spring(
                pan,
                {
                    toValue: { x: 0, y: 0 },
                    useNativeDriver: true
                } // Back to zero
            ).start();
        }
        function stop() {
            pan.setValue({
                x: 0,
                y: 0
            })
        }
        const yPos = gestureState.moveY;
        const xPos = gestureState.moveX;

        // Check if it is in the foundation zone

        if (yPos < 350 && xPos < 150) {
            // Check if last card is played or more than one
            if (playCardIndex === cards.piles[playPileIndex].length - 1) {
                const res = playPileToFoundation(cards, playPileIndex);
                if (res.valid) {
                    stop();
                    setCards({ ...res.cards })
                    return;
                } else {
                    springAnim();
                    return;
                }
            }
        }
        // Pile zone
        // Substract margin (60), divide card length (60) + bottom margin (6)
        // to figure out which pile 

        const targetPileIndex = Math.floor((yPos - 60) / 66)
        const isPileValid = targetPileIndex >= 0 && targetPileIndex <= 6;

        if (!isPileValid) {
            springAnim();
            return;
        }
        const res = playPileToPile(cards, playPileIndex, playCardIndex, targetPileIndex);

        if (!res.valid) {
            springAnim();
            return;
        }
        stop();
        setCards({ ...res.cards })
    }

    function renderOpenStock() {
        const lastThree = cards.stock.open.slice(-3)
        const length = lastThree.length //in case there is one or two cards
        const elements = lastThree.map((card, index) => (
            index === length - 1 ?
                <Draggable key={card.suit + '-' + card.rank} onRelease={handleStockRelease}>
                    <View style={styles.stagger}>
                        <Card suit={card.suit} rank={card.rank} hidden={card.hidden} />
                    </View>
                </Draggable>
                :
                <View key={card.suit + '-' + card.rank} style={styles.stagger}>
                    <Card suit={card.suit} rank={card.rank} hidden={card.hidden} />
                </View>
        ))
        return elements
    }

    function renderPile(pileIndex) {
        let zIndex = 100;
        const pile = cards.piles[pileIndex];
        //reversing array to stack elements in each other
        //to allow group dragging
        if (pile.length === 0) {
            return <View style={[styles.empty, styles.stagger]}></View>;
        }
        const el = pile.slice().reverse().reduce((acc, card, reversedCardIndex) => {
            const trueCardIndex = pile.length - 1 - reversedCardIndex;
            const el = <Draggable
                key={card.suit + '-' + card.rank + "-" + card.hidden.toString()}
                disabled={card.hidden}
                onRelease={(ev, gestureState, pan) => {
                    handlePileRelease(ev, gestureState, pan, trueCardIndex, pileIndex);
                    
                }}
                style={{ flexDirection: 'row', zIndex }}>

                {/* Put already existing elements here, if any */}
                {!!acc && acc}

                <View style={styles.stagger}>
                    <Card suit={card.suit} rank={card.rank} hidden={card.hidden} />
                </View>
            </Draggable>

            zIndex -= 1;
            return el;
        }, false);
        return el;
    }
    function renderFoundations() {
        return cards.foundations.map((foundation, index) => {
            if (foundation.length === 0) {
                return (
                    <View key={index} opacity={.3} style={styles.foundation}>
                        <Card suit={index + 1} />
                    </View>
                )
            } else {
                return (
                    <View
                        key={index.toString() + foundation[foundation.length - 1].rank.toString()}
                        style={styles.foundation}>
                        <Card key={index} suit={index + 1} rank={foundation[foundation.length - 1].rank} />
                    </View >
                )
            }
        })
    }
    return (
        <View style={styles.container}>
            <View style={styles.topContainer}>
                <View style={styles.foundationContainer}>
                    {renderFoundations()}
                </View>
                <View style={styles.pilesContainer}>
                    {cards.piles.map((pile, index) =>
                        <View key={index.toString()} style={styles.pileContainer}>
                            {renderPile(index)}
                        </View>
                    )}
                </View>
            </View>

            <View style={styles.stockContainer}>
                <TouchableWithoutFeedback onPress={handleDeal}>
                    {cards.stock.closed.length === 0 ?
                        <View style={styles.empty}></View>
                        :
                        <View style={styles.stockClosed}>
                            {cards.stock.closed.slice(0, 3).map((card, index) => (
                                <View key={card.suit + '-' + card.rank} style={styles.stagger}>
                                    <Card suit={card.suit} rank={card.rank} hidden={card.hidden} />
                                </View>))}
                        </View>}
                </TouchableWithoutFeedback>
                <View style={styles.stockOpen}>
                    {renderOpenStock()}
                </View>
            </View>
        </View >
    );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingTop: 60,
        paddingBottom: 24,
        justifyContent: 'space-between'
    },
    topContainer: {
        flexDirection: 'row',
    },
    foundationContainer: {
        width: 100,
        marginLeft: 6,
    },
    foundation: {
        marginBottom: 12,
    },
    pilesContainer: {
        flex: 1,
    },
    stockContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginLeft: 6,
        marginRight: 36,
    },
    stockClosed: {
        flexDirection: 'row',
    },
    stockOpen: {
        flexDirection: 'row-reverse',
    },
    empty: {
        width: 48,
        height: 60,
        backgroundColor: 'lightgrey',
        borderRadius: 3,
        marginLeft: 12,
    },
    stagger: {
        marginRight: -30,
    },
    pileContainer: {
        flexDirection: 'row-reverse',
        marginRight: 36,
        marginLeft: 36,
        marginBottom: 6,
    },
});

100 - 60 / 66
