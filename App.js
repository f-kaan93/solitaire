import { StatusBar } from 'expo-status-bar';
import React from 'react';
import Main from './src/Main';
import { StyleSheet, View } from 'react-native';


export default function App() {
  return (
    <View style={styles.container}>
      <Main />
      <StatusBar style="auto" />
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
      flex: 1
  },
});