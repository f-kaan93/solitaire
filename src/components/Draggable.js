import React from 'react';
import { Animated, PanResponder, ShadowPropTypesIOS, Text } from 'react-native';

/*

Props:

onRelease: Function
children: React elements
disabled: bool

*/

export default function Draggable(props) {
    const pan = React.useRef(new Animated.ValueXY()).current;
    const panResponder = React.useRef(
        PanResponder.create({
            onStartShouldSetPanResponder: () => true,
            onPanResponderMove: (evt, gestureState) => {
                Animated.event(
                    [
                        null,
                        { dx: pan.x, dy: pan.y }
                    ],
                    { useNativeDriver: false }
                )(evt, gestureState)
            },
            onPanResponderRelease: (evt, gestureState) => {
                props.onRelease(evt, gestureState, pan)
            },
        })
    ).current;

    if (props.disabled) {
        panResponder.panHandlers = undefined
    }

    return (
        <Animated.View
            style={{
                transform: [{ translateX: pan.x }, { translateY: pan.y }], ...props.style,
            }}
            {...panResponder.panHandlers}
        >
            {props.children}
        </Animated.View>
    );
};