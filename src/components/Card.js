import React from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';

/*

Props:

suit (int): 1 to 4 sorted alphebatically
    1: clubs
    2: diamonds
    3: hearts
    4: spades

rank (int): 1 to 13
    1: ace
    2 to 10 as is
    11: jack
    12: queen
    13: king

hidden (bool)
*/
const suitImages = [
    require('../../assets/suit_club.png'),
    require('../../assets/suit_diamond.png'),
    require('../../assets/suit_heart.png'),
    require('../../assets/suit_spade.png'),
]
const backImage = require('../../assets/card_back.png');

const renderSuit = (suit) => {
    
    // Substract 1 for 0 indexing
    const image = suitImages[suit - 1]
    return <Image style={styles.suit} source={image} />
}

const renderRank = (rank, suit) => {
    rank = ['A', 2, 3, 4, 5, 6, 7, 8, 9, 10, 'J', 'Q', 'K'][rank - 1];
    const textColor = ['blackText', 'redText', 'redText', 'blackText'][suit - 1];
    return <Text style={[styles.rank, styles[textColor]]}> {rank}</Text >
}

export default function Card(props) {
    return props.hidden ?
        <View style={styles.back}>
            <Image style={styles.cover} source={backImage} />
        </View>
        :
        <View style={styles.front}>
            {renderRank(props.rank, props.suit)}
            {renderSuit(props.suit)}
            <Text></Text>{/* Empty text to center suit*/}
        </View>
}

const styles = StyleSheet.create({
    front: {
        width: 48,
        height: 60,
        backgroundColor: 'white',
        borderRadius: 6,
        alignItems: 'center',
        justifyContent: 'center',
        shadowColor: '#ccc',
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 2,
        shadowOpacity: 1,
    },
    back: {
        width: 48,
        height: 60,
        borderRadius: 6,
        backgroundColor: 'white',
        shadowColor: '#ccc',
        alignItems: 'center',
        justifyContent: 'center',
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 2,
        shadowOpacity: 1,
    },

    rank: {
        marginLeft: 24,
        fontWeight: '700',
    },
    cover: {
        width: 48-8,
        height: 60-8,
    },
    redText: {
        color: '#BB2020',
    },
    blackText: {
        color: '#06111C',
    },
    suit: {
        width: 18,
        height: 18,
        margin: 6,
        resizeMode: 'stretch'
    }
});