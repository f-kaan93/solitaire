function generateCards() {
    // returns an ordered array of cards {suit: int, rank: int, hidden: bool}[]
    const suits = [1, 2, 3, 4];
    const ranks = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
    const cards = suits.map(suit => ranks.map(rank => ({ suit, rank, hidden: true }))).flat()
    return cards;
}

function shuffle(cards) {
    var currentIndex = cards.length, randomIndex;

    while (0 !== currentIndex) {

        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;

        [cards[currentIndex], cards[randomIndex]] = [
            cards[randomIndex], cards[currentIndex]];
    }

    return cards;
}



function suitCheck(suit1, suit2) {
    /* 
    clubs(black)   1
    diamond(red)   2
    heart(red)     3
    spade(black)   4

    1+1=2   not valid (same)
    1+2=3   valid
    1+3=4   valid
    1+4=5   not valid
    2+2=4   not valid (same)
    2+3=5   not valid
    2+4=6   valid
    3+3=6   not valid (same)
    3+4=7   valid
    4+4=8   not valid (same)

    not valid if same, not valid if sum is 5
    valid otherwise

    */
    if (suit1 === suit2) {
        return false;
    }
    if (suit1 + suit2 === 5) {
        return false;
    }
    return true;
}



export function initialize() {
    /*

    Returns a shuffled ready to play cards object;

    {
        piles: {suit: int, rank: int, hidden: bool}[],
        stock: {
            open: {suit: int, rank: int, hidden: bool}[],
            closed: {suit: int, rank: int, hidden: bool}[]
        },
        foundations: [
             {suit: int, rank: int, hidden: bool}[],  clubs
             {suit: int, rank: int, hidden: bool}[],  diamonds
             {suit: int, rank: int, hidden: bool}[],  hearts
             {suit: int, rank: int, hidden: bool}[]   spades
        ]
    }


    */
    let piles = [];
    let stock = {};
    const foundations = [[], [], [], []];
    let cards = shuffle(generateCards());
    for (let i = 1; i < 8; i++) {
        piles.push(cards.splice(0, i));
    }
    piles.forEach(pile => { pile[pile.length - 1].hidden = false });
    stock.open = cards.splice(0, 3);
    stock.open.forEach(card => { card.hidden = false })
    stock.closed = cards;
    return { piles, stock, foundations }
}


export function deal(stock) {
    if (stock.closed.length === 0) {
        stock.closed = stock.open;
        stock.open = [];
    } else {
        stock.open = stock.open.concat(stock.closed.splice(0, 3))
    }
    stock.open.forEach(card => { card.hidden = false })
    stock.closed.forEach(card => { card.hidden = true })
    return stock;
}

export function playStockToPile(cards, pileIndex) {
    const stockCard = cards.stock.open[cards.stock.open.length - 1];
    if (cards.piles[pileIndex].length === 0) {
        cards.stock.open.pop();
        cards.piles[pileIndex].push(stockCard);
        return {
            valid: true,
            cards,
        }
    }
    const pileCard = cards.piles[pileIndex][cards.piles[pileIndex].length - 1];
    if (stockCard.rank + 1 === pileCard.rank && suitCheck(stockCard.suit, pileCard.suit)) {
        cards.stock.open.pop();
        cards.piles[pileIndex].push(stockCard);
        return {
            valid: true,
            cards,
        }
    } else {
        return {
            valid: false,
            cards,
        }
    }
}
export function playStockToFoundation(cards) {
    const stockCard = cards.stock.open[cards.stock.open.length - 1];
    const foundation = cards.foundations[stockCard.suit - 1];
    //Foundations array is 0 indexed, card ranks and suits are 1
    if (foundation.length === 0) {
        if (stockCard.rank === 1) {
            cards.foundations[stockCard.suit - 1].push(stockCard);
            cards.stock.open.pop();
            return {
                valid: true,
                cards
            }
        }
        return {
            valid: false,
            cards
        }
    }
    const foundationLastCard = foundation[foundation.length - 1]
    console.log(foundation)
    if (foundationLastCard.rank + 1 === stockCard.rank) {
        cards.foundations[stockCard.suit - 1].push(stockCard);
        cards.stock.open.pop();
        return {
            valid: true,
            cards
        }
    }
    return {
        valid: false,
        cards
    }
}

export function playPileToPile(cards, playPileIndex, playCardIndex, targetPileIndex) {
    function moveCardsToTarget() {
        // Moves the cards from the play pile to target pile,
        // shows the hidden card
        cards.piles[targetPileIndex] = cards.piles[targetPileIndex].concat(
            cards.piles[playPileIndex].splice(playCardIndex, cards.piles[playPileIndex].length)
        )
        if (cards.piles[playPileIndex].length > 0) {
            cards.piles[playPileIndex][cards.piles[playPileIndex].length - 1].hidden = false;
        }
    }

    const playCard = cards.piles[playPileIndex][playCardIndex];

    if (cards.piles[targetPileIndex].length === 0) {
        moveCardsToTarget()
        return {
            valid: true,
            cards,
        }
    }

    const targetCard = cards.piles[targetPileIndex][cards.piles[targetPileIndex].length - 1];
    if (playCard.rank + 1 === targetCard.rank && suitCheck(playCard.suit, targetCard.suit)) {
        moveCardsToTarget()
        return {
            valid: true,
            cards,
        }
    } else {
        return {
            valid: false,
            cards,
        }
    }
}

export function playPileToFoundation(cards, pileIndex) {
    const pileCard = cards.piles[pileIndex][cards.piles[pileIndex].length - 1];
    const foundation = cards.foundations[pileCard.suit - 1];
    //Foundations array is 0 indexed, card ranks and suits are 1
    if (foundation.length === 0) {
        if (pileCard.rank === 1) {
            cards.foundations[pileCard.suit - 1].push(pileCard);
            cards.piles[pileIndex].pop();
            if (cards.piles[pileIndex].length > 0) {
                cards.piles[pileIndex][cards.piles[pileIndex].length - 1].hidden = false;
            }
            return {
                valid: true,
                cards
            }
        }
        return {
            valid: false,
            cards
        }
    }
    const foundationLastCard = foundation[foundation.length - 1]
    if (foundationLastCard.rank + 1 === pileCard.rank) {
        cards.foundations[pileCard.suit - 1].push(pileCard);
        cards.piles[pileIndex].pop();
        cards.piles[pileIndex][cards.piles[pileIndex].length - 1].hidden = false;
        return {
            valid: true,
            cards
        }
    }
    return {
        valid: false,
        cards
    }
}